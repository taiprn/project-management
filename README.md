# Project eRisk #

### Coordinator ###

  -Razvan Benchea

### Members ###

  -Cosmina Asofiei: cosminaasofiei@gmail.com  
  -Alexandru Rusu: alexrusu96@gmail.com  
  -Stan Rares: stan.rares@gmail.com  
  -Cristian Ion: cristian.ion94@gmail.com

#### Description ####

  Project eRisk is an application which uses neural networks to determine the emotional state of the user using the information from the messaging services he uses. If the user enters a depressive or other self-harming emotional states the application will notify the user or the user's contact person.

#### Used technologies ####

  - for the Back-End we will use python with the django framework;
  - for the Front-End we will use the Angular framework, with Angular Material as the design framework;
  - for the neural network implementation, training and use we will use the tensorflow framework;
